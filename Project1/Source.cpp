#pragma once
#include<iostream>
#include"GameManager.h"
using namespace std;

/* 
A* Algortihm by Srinivasan and Meith.

Citations:

For doubts and debugging:

www.cplusplus.com

www.stackoverflow.com

For concepts:

Youtube Channel : CodeCompile Video: A* Pathfinding Tutorial Part 1 and 2
*/

int main()
{
	GameManager gameManager;
	gameManager.gameStart();
	
	return 0;
}

