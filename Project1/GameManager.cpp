#include "GameManager.h"
#include "PathFinding.h"

GameManager::GameManager()
{
	animIT = 0;
}


GameManager::~GameManager()
{
}

void GameManager::gameStart()
{
	list<point> goals;
	char inputString[wallnumber+20];
	
	std::cout << "\n First: Maximize command prompt window.\n Enter a String with NO SPACES use _ instead ( max Size: "<< wallnumber<<" ) : ";
	cin>>inputString;
	//this function the prints the map onto map.txt file for future reference
	textMap();
	sample.drawGrid();
	cout << "\nGrid before randomizing string.\n";
	system("pause");
	
	// buildmap converts the input string to a set of destination points and transfers the list to goals
	goals = buildMap(inputString);
	sample._pathfinding.initGrid();
	_leftGoals = goals;
	// draw the map after the string is randomized
	sample.drawGrid();
	cout << "Grid after randomizing.\n";
	system("pause");

	//set the entry for the string
	point s, d;
	s.x = 3;
	s.y = 3;
	s.f = 0;
	int i = 0;
	list<point>::iterator it;
	system("CLS");
	sample.drawGrid();
	//execute the loop till all the goals are reached
	for (it = goals.begin(); it != goals.end();it ++, i++)
	{	
		sample._done = false;
		sample._pathfinding.initGrid();
		updateGoals();

		d= findGoal(inputString[i]);
		s.f = 0;
		sample.findPath(s, d);
		s = d;
		
		if (sample._done == true)
		{
			_moveQ.clear();
			_moveQ = sample.drawPath();
			animate();
			sample._pathfinding.grid[d.x][d.y] = 0;
			editGrid(d.x, d.y, ' ');
		}
		else
		{
			//The snake is unable to find a path and is trapped
			std::cout << "\n\nThe Snake is DEAD !!!!!!";
			break;
		}
	}

	system("CLS");
	editGrid(10, 0, ' ');
	std::cout << "The Program has ENDED !!!!";
	editGrid(15, 0, ' ');
	system("pause");
}

void GameManager::animate()
{
	//animate to movement of the snake from source to destination
	//system("CLS");
	//sample.drawGrid();
	int i;
	std::list<point>::iterator it;
	
	for (it = _moveQ.begin(); it != _moveQ.end(); it++, animIT++)
	{	
		i = animIT;
		i = i % 4;
		
		editGrid(snake[i].x, snake[i].y,' ');
		snake[i] = *it;
		
		editGrid(snake[i].x, snake[i].y,'*');
		Sleep(50);
	}
}

void GameManager::editGrid(int x, int y,char symbol)
{
	//function to change a charachter at a specific position
	COORD pos = { (2*y), (x*2) +1  };
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
	cout << symbol;
}

list<point> GameManager::buildMap(char str[])
{
	//function to randomize the string and buld a list of destination
	int x, y;
	list<point> goals;
	point goalPoints;
	for (int i = 0; i < strlen(str); i++)
	{
		
		do
		{
			x = rand() % (gridSize - 3) + 1;
			y = rand() % (gridSize - 3) + 1;

		} while (sample._pathfinding.grid[x][y] != 0);

		sample._pathfinding.grid[x][y] = str[i];
		
		goalPoints.x = x;
		goalPoints.y = y;
		goals.push_back(goalPoints);
	}

	

	return goals;
}

void GameManager::textMap()
{
	//function to print the map onto the text file
	ofstream myFile;
	myFile.open("map.txt");
	
	for (int i = 0; i < gridSize; i++)
	{
		for (int j = 0; j < gridSize; j++)
		{
			if (sample._pathfinding.grid[i][j] == 0)
				myFile << "  ";
			else
				myFile << 'X' << " ";
		}
		myFile << endl;
	}
	myFile.close();
}

point GameManager::findGoal(char x)
{
	point closest;
	closest.h = 99;
	list<point>::iterator it;
	
	for (it = _leftGoals.begin(); it != _leftGoals.end(); it++)
	{
		if ((sample._pathfinding.grid[it->x][it->y] == x) && (sample._pathfinding.Grid[it->x][it->y].h < closest.h))
		{
			closest = *it;
		}
	}
	//sample._pathfinding.grid[closest.x][closest.y] = 0;
	_leftGoals.remove(closest);
	return closest;
}

void GameManager::updateGoals()
{
	list<point>::iterator it;

	for (it = _leftGoals.begin(); it != _leftGoals.end(); it++)
	{
		*it = sample._pathfinding.Grid[it->x][it->y];
	}
}