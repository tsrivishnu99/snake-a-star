#include "PathFinding.h"
#include<iostream>
#include<ctime>

PathFinding::PathFinding()
{
	srand(time(NULL));

	int x, y;

	//create the walls of the map
	for (int i = 0; i < gridSize; i++)
	{
		for (int j = 0; j < gridSize; j++)
			grid[i][j] = 0;

		grid[i][gridSize - 1] = wallAscii;
		grid[i][0] = wallAscii;
		grid[0][i] = wallAscii;

	}

	//the bottom most wall of the map
	for (int i = 0; i < gridSize; i++)
		grid[gridSize - 1][i] = wallAscii;

	//produce obstacles at random coordinates, thus producing a different map every time
	for (int i = 0; i < wallnumber; i++)
	{
		//to ensure that the walls do not overlap
		do
		{
			x = rand() % (gridSize - 2) + 1;
			y = rand() % (gridSize - 2) + 1;


		} while (grid[x][y] != 0 && grid[x + 1][y] != wallAscii && grid[x - 1][y] != wallAscii && grid[x][y + 1] != wallAscii && grid[x][y-1] != wallAscii);

		grid[x][y] = wallAscii;
	}
}


PathFinding::~PathFinding()
{
}


void PathFinding::initGrid()
{
	
	//generate the actual back-end of the map using the buffer grid

	for (int i = 1; i < gridSize; i++)
	{
		for (int j = 1; j < gridSize; j++)
		{
			Grid[i][j].x = i;
			Grid[i][j].y = j;
			
			Grid[i][j].g = 0;
			Grid[i][j].f = -1;
			Grid[i][j].h = heuristic(i, j);

			//to check if the points are valid/movable points or invalid/walls.
			Grid[i][j].up = isNodeValid(i - 1, j);
			Grid[i][j].down = isNodeValid(i+1, j);
			Grid[i][j].right = isNodeValid(i , j+1);
			Grid[i][j].left = isNodeValid(i, j-1);
			
		}
	}
}

//function to find the heuristic value of each point
int PathFinding::heuristic(int i, int j)
{
	int h = 0;
	h = abs(i - destination.x) + abs(j - destination.y);
	return h;
}
//to check if the points are valid/movable points or invalid/walls.
point* PathFinding::isNodeValid(int i, int j)
{
	if (grid[i][j] == 0 || grid[i][j] == grid[destination.x][destination.y])
	{
		return &Grid[i][j];
	}
	else
	{
		return nullptr;
	}
}
