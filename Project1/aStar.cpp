#include "aStar.h"


aStar::aStar()
{
	_done = false;
}


aStar::~aStar()
{
}

// the function to find the path from source to destination
void aStar::findPath(point source, point destination)
{
	//ensure that the openset and closed set are empty
	flushBuffers();
	
	//find the source in the MAP and set its value to self as 0
	_pathfinding.Grid[source.x][source.y].f = 0;
	_pathfinding.source = _pathfinding.Grid[source.x][source.y];
	_pathfinding.destination = _pathfinding.Grid[destination.x][destination.y];
	
	//initialize Grid; each point knows how far it is from the destination
	_pathfinding.initGrid();
	
	//push source onto openSet
	addToOpenSet(_pathfinding.source);
	
	point currentNode, anyPoint;
	while (!openSet->isEmpty(openSet))
	{
		//get the lowest point in the openSet and push all tis neighbours to openSet
		currentNode = lowestPoint();
		if (currentNode == _pathfinding.destination)
		{
			_done = true;
			drawPath();
			return;
		}
		else
		{
			//uncomment the below code for information about traversed nodes in the text document but it will slow the program (not advised) 

			//nodesTraversed('b');

			//pop from openSet and push to closed Set
			pushNeighborIntoOpenSet(currentNode);
			addToClosedSet(currentNode);
			popFromOpenSet(currentNode);
			
			//nodesTraversed('a');

		}
	}
	
	if (_done != true)
	{
		cout << "Path Not found";
		return;
	}

}



#pragma region helper Functions

void aStar::addToOpenSet(point pt)
{
	openSet= openSet->insert(openSet, pt);
}
void aStar::popFromOpenSet(point pt)
{
	openSet = openSet->remove(openSet, pt);
}
void aStar::addToClosedSet(point pt)
{
	closedSet = closedSet->insert(closedSet, pt);
}
int aStar::calcFvalue(point pt)
{
	return pt.g + pt.h;
}

bool aStar::isInClosedSet(point pt)
{
	if (closedSet->search_n(closedSet, pt))
		return true;
	else
		return false;
}

point aStar::lowestPoint()
{
	point lowest = (openSet->findmin(openSet))->node;
	return lowest;
}

void aStar::pushNeighborIntoOpenSet(point currentNode)
{
	point* neighbors[4];
	point *x;
	neighbors[0] = currentNode.up;
	neighbors[1] = currentNode.down;
	neighbors[2] = currentNode.left;
	neighbors[3] = currentNode.right;
	int fValue;
	for (int i = 0; i < 4; i++)
	{
		if (neighbors[i] != nullptr && !isInClosedSet(*neighbors[i]))
		{
			//update the neighbour node's information and push it onto the open Set
			x = &_pathfinding.Grid[neighbors[i]->x][neighbors[i]->y];
			neighbors[i]->g = currentNode.g + movementCost;
			fValue = calcFvalue(*neighbors[i]);
			if (fValue < x->f || x->f == (-1))
			{
				x->g = neighbors[i]->g;
				x->f = fValue;
				x->parentPoint = &_pathfinding.Grid[currentNode.x][currentNode.y];
			}
			addToOpenSet(*x);
		}
	}

}

//function to retrace the path back to the source from destination and push the list of the node to moves-queue
list<point> aStar::drawPath()
{
	point* currentNode;
	list<point> moveQ;
	if (_done == true)
	{

		currentNode = &_pathfinding.Grid[_pathfinding.destination.x][_pathfinding.destination.y];
		currentNode = currentNode->parentPoint;

		while (!(*currentNode == _pathfinding.source))
		{
			moveQ.push_front(_pathfinding.Grid[currentNode->x][currentNode->y]);
			currentNode = currentNode->parentPoint;
		}

	}
	return moveQ;
}

//empty the buffers
void aStar::flushBuffers()
{
	
	if (!openSet->isEmpty(openSet))
		openSet->clear(openSet);
	if (!closedSet->isEmpty(closedSet))
		closedSet->clear(closedSet);
	
	openSet = nullptr;
	closedSet = nullptr;
}

//print the list of traversed nodes in to the file traversedNodes.txt
void aStar::nodesTraversed(char p)
{
	ofstream myFile;
	myFile.open("traversedNodes.txt", std::ios_base::app | std::ios_base::out);
	switch (p)
	{
	case 'b':
		myFile << "openset (before): \n";
		openSet->inorder(openSet);
		myFile << "closed set(before)\n";
		closedSet->inorder(closedSet);
		break;
	case 'a':
		myFile << "openset (after): \n";
		openSet->inorder(openSet);
		myFile << "closed set(after)\n";
		closedSet->inorder(closedSet);
		break;
	}
	myFile.close();
}

void aStar::drawGrid()
{
	ofstream myFile;
	myFile.open("output.txt", std::ios_base::app | std::ios_base::out);
	//draw the grid on the console screen
	myFile << endl;
	cout << endl;

	for (int i = 0; i < gridSize; i++)
	{
		for (int j = 0; j < gridSize; j++)
		{
			if (_pathfinding.grid[i][j] == 0)
			{
				myFile <<" ";
				cout << "  ";
			}
			else
			{
				myFile << _pathfinding.grid[i][j] << " ";
				cout << _pathfinding.grid[i][j] << " ";
			}
		}
		myFile << endl;
		cout << endl;
	}
	myFile.close();
}



#pragma endregion