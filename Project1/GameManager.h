#pragma once
#include"aStar.h"
#include<fstream>
#include<Windows.h>
#include<string>
#define snakeLength 4

using namespace std;

// Main handle of the entire program
class GameManager
{
public:
	GameManager();
	~GameManager();
	
	void gameStart();

private:
	void textMap();
	aStar sample;
	int animIT;
	point snake[snakeLength];
	void editGrid(int , int , char);
	list<point> _moveQ;
	void animate();
	list<point> buildMap(char[]);
	list<point> _leftGoals;
	point findGoal(char);
	void updateGoals();
};

