//#include "AVL.h"
//
//template <class T>
//AVL<T>::AVL(T k)
//{
//	key = k;
//	left = right = 0;
//	height = 1;
//}
//
//template <class T>
//T AVL<T>::getheight(AVL* p)
//{
//	return p ? p->height : 0;
//}
//
//template <class T>
//T AVL<T>::bfactor(AVL* p)
//{
//	return getheight(p->right) - getheight(p->left);
//}
//
//template <class T>
//void AVL<T>::fixheight(AVL* p)
//{
//	T hl = getheight(p->left);
//	T hr = getheight(p->right);
//	p->height = (hl>hr ? hl : hr);
//}
//
//template <class T>
//AVL<T>* AVL<T>::rotateright(AVL* p)
//{
//	AVL* q = p->left;
//	p->left = q->right;
//	q->right = p;
//	fixheight(p);
//	fixheight(q);
//	return q;
//}
//
//template <class T>
//AVL<T>* AVL<T>::rotateleft(AVL* p)
//{
//	AVL* q = p->right;
//	p->right = q->left;
//	q->left = p;
//	fixheight(p);
//	fixheight(q);
//	return q;
//}
//
//template <class T>
//AVL<T>* AVL<T>::balance(AVL* p)
//{
//	fixheight(p);
//	if (bfactor(p) == 2)
//	{
//		if (bfactor(p->right) < 0)
//		{
//			p->right = rotateright(p->right);
//		}
//		return rotateleft(p);
//	}
//	if (bfactor(p) == -2)
//	{
//		if (bfactor(p->left) < 0)
//		{
//			p->left = rotateleft(p->left);
//		}
//		return rotateright(p);
//	}
//	return p;
//}
//
//template <class T>
//AVL<T>* AVL<T>::insert(AVL* p, T k)
//{
//	if (!p)
//	{
//		return new AVL(k);
//	}
//	if (k<p->key)
//	{
//		p->left = insert(p->left, k);
//	}
//	else
//	{
//		p->right = insert(p->right, k);
//	}
//	return balance(p);
//}
//
//template <class T>
//AVL<T>* AVL<T>::findmin(AVL* p)
//{
//	return p->left ? findmin(p->left) : p;
//}
//
//template <class T>
//AVL<T>* AVL<T>::removemin(AVL* p)
//{
//	if (p->left == 0)
//	{
//		return p->right;
//	}
//	p->left = removemin(p->left);
//	return balance(p);
//}
//
//
//template <class T>
//bool AVL<T>::isEmpty(AVL* p)
//{
//	if (p == nullptr)
//	{
//		return true;
//	}
//	else 
//		return false;
//}
//
//template <class T>
//AVL<T>* AVL<T>::remove(AVL* p, T k)
//{
//	if (!p)
//	{
//		return 0;
//	}
//	if (k < p->key)
//	{
//		p->left = remove(p->left, k);
//	}
//	else if (k > p->key)
//	{
//		p->right = remove(p->right, k);
//	}
//	else
//	{
//		AVL* q = p->left;
//		AVL* r = p->right;
//		delete p;
//		if (!r)
//		{
//			return q;
//		}
//		AVL* min = findmin(r);
//		min->right = removemin(r);
//		min->left = q;
//		return balance(min);
//	}
//	return balance(p);
//}
//
//template <class T>
//AVL<T>* AVL<T>::search_n(AVL* p, T k)
//{
//
//	if (!p)
//	{
//		return nullptr;
//	}
//	if (k < p->key)
//	{
//		p->left = search_n(p->left, k);
//	}
//	else if (k > p->key)
//	{
//		p->right = search_n(p->right, k);
//	}
//	else
//	{
//		return p;
//	}
//}
//
//
//template <class T>
//void AVL<T>::inorder(AVL* p)
//{
//
//	if (p)
//	{
//		inorder(p->left);
//		cout << p->key << " ";
//		inorder(p->right);
//	}
//}
//template <class T>
//void AVL<T>::clear(AVL* p)
//{
//	while(p != nullptr)
//	{
//		remove(p, p.findmin(&p));
//	}
//}
