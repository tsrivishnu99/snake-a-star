#pragma once
#include "PathFinding.h"
#include<iostream>
#include<fstream>
using namespace std;


template <class T>
class AVL
{
public:
	T key;
	unsigned char height;
	AVL<T>* left;
	AVL<T>* right;

	AVL();
	AVL(T);
	AVL<T>* insert(AVL*, T);
	AVL<T>* findmin(AVL*);
	AVL<T>* removemin(AVL*);
	AVL<T>* remove(AVL*, T);
	AVL<T>* search_n(AVL*, T);
	bool isEmpty(AVL* );
	void inorder(AVL*);
	void clear(AVL*);

private:
	unsigned char getheight(AVL*);
	unsigned char bfactor(AVL*);
	void fixheight(AVL*);
	AVL<T>* rotateright(AVL*);
	AVL<T>* rotateleft(AVL*);
	AVL<T>* balance(AVL*);
};

//=============================================================================================================================================
//==================================================== Function definitions ===================================================================
//=============================================================================================================================================
template <class T>
AVL<T>::AVL()
{
	cout << "we need this" << endl;
}

template <class T>
AVL<T>::AVL(T k)
{
	key = k;
	left = right = nullptr;
	height = 1;
}

template <class T>
unsigned char AVL<T>::getheight(AVL* p)
{
	return p ? p->height : 0;
}

template <class T>
unsigned char AVL<T>::bfactor(AVL* p)
{
	return getheight(p->right) - getheight(p->left);
}

template <class T>
void AVL<T>::fixheight(AVL* p)
{
	unsigned char hl = getheight(p->left);
	unsigned char hr = getheight(p->right);
	p->height = (hl>hr ? hl : hr);
}

template <class T>
AVL<T>* AVL<T>::rotateright(AVL* p)
{
	AVL* q = p->left;
	p->left = q->right;
	q->right = p;
	fixheight(p);
	fixheight(q);
	return q;
}

template <class T>
AVL<T>* AVL<T>::rotateleft(AVL* p)
{
	AVL* q = p->right;
	p->right = q->left;
	q->left = p;
	fixheight(p);
	fixheight(q);
	return q;
}

template <class T>
AVL<T>* AVL<T>::balance(AVL* p)
{
	fixheight(p);
	if (bfactor(p) == 2)
	{
		if (bfactor(p->right) < 0)
		{
			p->right = rotateright(p->right);
		}
		return rotateleft(p);
	}
	if (bfactor(p) == -2)
	{
		if (bfactor(p->left) < 0)
		{
			p->left = rotateleft(p->left);
		}
		return rotateright(p);
	}
	return p;
}

template <class T>
AVL<T>* AVL<T>::insert(AVL* p, T k)
{
	if (!p)
	{
		return new AVL(k);
	}
	if (k<p->key)
	{
		p->left = insert(p->left, k);
	}
	else
	{
		p->right = insert(p->right, k);
	}
	return balance(p);
}

template <class T>
AVL<T>* AVL<T>::findmin(AVL* p)
{
	return p->left ? findmin(p->left) : p;
}

template <class T>
AVL<T>* AVL<T>::removemin(AVL* p)
{
	if (p->left == nullptr)
	{
		return p->right;
	}
	p->left = removemin(p->left);
	return balance(p);
}


template <class T>
bool AVL<T>::isEmpty(AVL* p)
{
	if (p == nullptr)
	{
		return true;
	}
	else
		return false;
}

template <class T>
AVL<T>* AVL<T>::remove(AVL* p, T k)
{
	if (!p)
	{
		return 0;
	}
	if (k < p->key)
	{
		p->left = remove(p->left, k);
	}
	else if (k > p->key)
	{
		p->right = remove(p->right, k);
	}
	else
	{
		AVL* q = p->left;
		AVL* r = p->right;
		delete p;
		if (!r)
		{
			return q;
		}
		AVL* min = findmin(r);
		min->right = removemin(r);
		min->left = q;
		return balance(min);
	}
	return balance(p);
}

template <class T>
AVL<T>* AVL<T>::search_n(AVL* p, T k)
{

	if (!p)
	{
		return nullptr;
	}
	if (k < p->key)
	{
		p->left = search_n(p->left, k);
	}
	else if (k > p->key)
	{
		p->right = search_n(p->right, k);
	}
	else
	{
		return p;
	}
}


template <class T>
void AVL<T>::inorder(AVL* p)
{

	if (p)
	{
		inorder(p->left);
		cout << p->key << " ";
		inorder(p->right);
	}
}
template <class T>
void AVL<T>::clear(AVL* p)
{
	cout << "in clear" << endl;
	//if (p == nullptr)
		//return;
	if (p->left!=nullptr)
		clear(p->left);
	if (p->right!=nullptr)
		clear(p->right);
	delete p;
	cout << "out of clear" << endl;
}

//=================================================================================================================================
//========================================== Template Specialization ==============================================================
//=================================================================================================================================

//Since the template uses the class object itself as the key factor, we needed to implement template specialization to enuse the key is the f value of each node

template <>
class AVL < point >
{
public:
	int key;
	point node;
	unsigned char height;
	AVL<point>* left;
	AVL<point>* right;

	AVL()
	{
		cout << "needed\n";
		left = right = nullptr;
	}
	//useless constructor
	AVL(point pt)
	{
		key = pt.f;
		node = pt;
		height = 1;
		left = nullptr;
		right = nullptr;
	}

	//function insert called first
	AVL<point>* insert(AVL* p, point k)
	{

		if (!p)
		{
			return new AVL(k);
		}
		if (k.f < p->key)
		{
			p->left = insert(p->left, k);
		}
		else
		{
			p->right = insert(p->right, k);
		}
		return balance(p);
	}


	//find min function
	AVL<point>* removemin(AVL* p)
	{
		if (p->left == nullptr)
		{
			return p->right;
		}
		p->left = removemin(p->left);
		return balance(p);
	}

	AVL<point>* findmin(AVL* p)
	{
		return p->left ? findmin(p->left) : p;
	}

	//Remove function
	AVL<point>* remove(AVL* p, point k)
	{
		if (!p)
		{
			return 0;
		}
		if (k.f < p->key)
		{
			p->left = remove(p->left, k);
		}
		else if (k.f > p->key)
		{
			p->right = remove(p->right, k);
		}
		else
		{
			AVL* q = p->left;
			AVL* r = p->right;
			delete p;
			if (!r)
			{
				return q;
			}
			AVL* min = findmin(r);
			min->right = removemin(r);
			min->left = q;
			return balance(min);
		}
		return balance(p);
	}

	//search function
	AVL<point>* search_n(AVL* p, point k)
	{
		if (!p)
		{
			return nullptr;
		}
		if (k.f < p->key)
		{
			p->left = search_n(p->left, k);
		}
		else if (k.f > p->key)
		{
			p->right = search_n(p->right, k);
		}
		else
		{
			return p;
		}
	}

	bool isEmpty(AVL<point>* root)
	{
		if (root == nullptr)
		{
			return true;
		}
		else
			return false;
	}
	void inorder(AVL* p)
	{
		ofstream myFile;
		myFile.open("traversedNodes.txt");
		if (p)
		{
			inorder(p->left);
			myFile << p->key;
			myFile << ' ';
			inorder(p->right);
		}
		myFile << "\n";
		myFile.close();
	}



	void clear(AVL* p)
	{
		if (p == nullptr)
			return;
		if (p->left != nullptr)
			clear(p->left);
		if (p->right != nullptr)
			clear(p->right);
		delete p;
		return;
	}
	unsigned char getheight(AVL* p)
	{
		return p ? p->height : 0;
	}

	unsigned char bfactor(AVL* p)
	{
		return getheight(p->right) - getheight(p->left);
	}
	void fixheight(AVL* p)
	{
		unsigned char hl = getheight(p->left);
		unsigned char hr = getheight(p->right);
		p->height = (hl>hr ? hl : hr);
	}
	AVL<point>* rotateright(AVL* p)
	{
		AVL* q = p->left;
		p->left = q->right;
		q->right = p;
		fixheight(p);
		fixheight(q);
		return q;
	}
	AVL<point>* rotateleft(AVL* p)
	{
		AVL* q = p->right;
		p->right = q->left;
		q->left = p;
		fixheight(p);
		fixheight(q);
		return q;
	}
	AVL<point>* balance(AVL* p)
	{
		fixheight(p);
		if (bfactor(p) == 2)
		{
			if (bfactor(p->right) < 0)
			{
				p->right = rotateright(p->right);
			}
			return rotateleft(p);
		}
		if (bfactor(p) == -2)
		{
			if (bfactor(p->left) < 0)
			{
				p->left = rotateleft(p->left);
			}
			return rotateright(p);
		}
		return p;
	}


};
