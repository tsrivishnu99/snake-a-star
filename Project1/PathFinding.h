#pragma once
# define gridSize 40
# define wallnumber 10
#define wallAscii 1

//The primary Unit structure in the grid
struct point
{
	//location of the point also its coordinate value in the matrix
	int x;
	int y;
	
	int h;
	int f;
	int g;

	point* parentPoint;	// used to define from where the path is coming from
	//neighbouring points; Since we are making a sanke game which can only move up, down, left or right , we have not implemented diagonal movement. It can also be added if necessary
	point* up;
	point* down;
	point* left;
	point* right;

	
	//constructor to avoid invalid memory access function
	point()
	{
		x = 0;
		y = 0;
		h = 0;
		f = 0;
		g = 0;

		parentPoint = nullptr;
		up = nullptr;
		down = nullptr;
		left = nullptr;
		right = nullptr;
	}

	// Operator Overloaders
	bool operator==(const point& other)
	{
		if (x == other.x && y == other.y)
			return true;
		else
			return false;
	}

	bool operator<(const point& other)
	{
		if (f < other.f)
			return true;
		else
			return false;
	}

	bool operator>(const point& other)
	{
		if (f > other.f)
			return true;
		else
			return false;
	}

	void operator=(const point& other)
	{
		x = other.x;
		y = other.y;
		h = other.h;
		f = other.f;
		g = other.g;

		parentPoint = other.parentPoint;
		up = other.up;
		down = other.down;
		left = other.left;
		right = other.right;
	}
};

//The main MAP class : it contains the map, each unit cells, source, destination (at anygiven point) and buffer for display
class PathFinding
{
public:
	PathFinding();
	~PathFinding();
	
	//buffer used for display purposes only
	char grid[gridSize][gridSize];
	//actual map which hold the status of each point on the map
	point Grid[gridSize][gridSize];
	point source, destination;
	void initGrid();
	int heuristic(int ,int);
	point* isNodeValid(int, int);

};

