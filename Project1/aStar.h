#pragma once
#include<iostream>
#include<list>
#include<string>
#include "AVL.h"
#include<algorithm>
#include<fstream>
#define movementCost 1

using namespace std;
class aStar
{
public:
	aStar();
	~aStar();
	void findPath(point, point);
	void drawGrid();
	list<point> drawPath();
	PathFinding _pathfinding;
	bool _done;
	void flushBuffers();

private:
	AVL<point>* closedSet;
	AVL<point>* openSet;

	void addToOpenSet(point);
	void popFromOpenSet(point);
	void addToClosedSet(point);
	int calcFvalue(point);
	point lowestPoint();
	bool isInClosedSet(point pt);
	void pushNeighborIntoOpenSet(point);
	void nodesTraversed(char);
	
};

